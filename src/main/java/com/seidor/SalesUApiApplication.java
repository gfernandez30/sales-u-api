package com.seidor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SalesUApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalesUApiApplication.class, args);
	}

}
